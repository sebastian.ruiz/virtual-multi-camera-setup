﻿using System;
using System.IO;
using AirSimUnity;
using UnityEngine;
 
public class CameraCapture : MonoBehaviour
{
    // public int fileCounter;
    public KeyCode captureKey;
    // public KeyCode captureKeyWithSegmentation;
    public KeyCode captureKeyBBs;
    
    private Camera[] cameras;

    private int fileCounter;

    private void Start()
    {
        fileCounter = 0;
        cameras = new Camera[5];
        int i = 0;
        foreach (Camera c in Camera.allCameras)
        {
            if (c.gameObject.name != "MainCamera")
            {
                cameras[i] = c;
                i++;
            }
        }
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(captureKey))
        {
            // ResetCounter();
            Capture(false, "scene", false);
            Capture(true, "scene", false);
        }
        // if (Input.GetKeyDown(captureKeyWithSegmentation))
        // {
        //     ResetCounter();
            
        // }
        if (Input.GetKeyDown(captureKeyBBs))
        {
            GetBoundingBoxes();
        }
    }

    public void ResetCounter()
    {
        fileCounter = 0;
    }
 
    public void Capture(bool instanceSegmentation = true, String captureDirectory = "", bool inNanoseconds = true)
    {
        String time = fileCounter.ToString();
        if (inNanoseconds)
        {
            double timeNanoseconds = System.DateTime.UtcNow.Subtract(
            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            ).TotalMilliseconds * 1000000;
            time = timeNanoseconds.ToString("F19").Split('.')[0];
            // Debug.Log(time);    
        }

        int num_camera = 0;
        foreach (Camera c in cameras)
        {
            Debug.Log("camera " + num_camera + " projection matrix");
            Debug.Log(c.projectionMatrix);
            // Debug.Log(c.worldToCameraMatrix);

            CameraFiltersScript cameraFiltersScript = c.gameObject.GetComponent<CameraFiltersScript>();
            
            if (!instanceSegmentation)
            {
                Debug.Log("disabling instance segmentation");
                cameraFiltersScript.ResetCameraEffects();
                
                // c.gameObject.GetComponent<CameraFiltersScript>().enabled = false;
                // c.renderingPath = RenderingPath.UsePlayerSettings;
                // c.clearFlags = CameraClearFlags.Skybox;
                // c.SetReplacementShader(null, null);
            }
            
            
            // RenderTexture activeRenderTexture = texture;
            // RenderTexture.active = texture;
            RenderTexture activeRenderTexture = RenderTexture.active;
            RenderTexture.active = c.targetTexture;
            
     
            c.Render();
     
            Texture2D image = new Texture2D(c.targetTexture.width, c.targetTexture.height);
            image.ReadPixels(new Rect(0, 0, c.targetTexture.width, c.targetTexture.height), 0, 0);
            image.Apply();
            RenderTexture.active = activeRenderTexture;
     
            byte[] bytes = image.EncodeToPNG();
            Destroy(image);

            String dir = Application.dataPath + "/../CameraScreenshots/" + captureDirectory + "/";
                
            String folder_path = dir + "cam" + num_camera;
            
            if (!Directory.Exists(folder_path))
            {
                Directory.CreateDirectory(folder_path);
            }
            String file_path = dir + "cam" + num_camera + "/" + time;
            if (instanceSegmentation)
            {
                file_path += ".is.png";
            }
            else
            {
                file_path += ".color.png";
            }

            File.WriteAllBytes(file_path, bytes);
            
            // re-enable instance segmentation
            if (!instanceSegmentation)
            {
                // c.gameObject.GetComponent<CameraFiltersScript>().enabled = true;
                cameraFiltersScript.SetSegmentationEffect();
            }
            
            num_camera++;
        }
        if (instanceSegmentation)
        {
            fileCounter++; // only advance counter after doing instance segmentation
        }
        
    }

    public void GetBoundingBoxes()
    {
        COCODataset cocoDataset = gameObject.GetComponent<COCODataset>(); 
        cocoDataset._CocoData = new CocoData();

        DateTime dateNow = System.DateTime.Now;
        cocoDataset._CocoData.info.version = "1.0";
        cocoDataset._CocoData.info.contributor = "Sebastian Ruiz";
        cocoDataset._CocoData.info.date_created = dateNow;
        cocoDataset._CocoData.info.year = 2020;
        cocoDataset._CocoData.info.description = "Bounding boxes for objects from Unity virtual environment.";

        GameObject Objects = GameObject.Find("Objects");
        
        int num_camera = 0;
        foreach (Camera c in cameras)
        {
            CocoImage image = new CocoImage();
            image.file_name = num_camera + ".png";
            image.id = num_camera;
            image.width = 1024;
            image.height = 768;
            image.date_captured = dateNow;
            cocoDataset._CocoData.images.Add(image);
            
            Debug.Log("Camera num:" + num_camera);
            foreach (Transform child in Objects.transform)
            {
                if (child.tag == "Cube" && child.gameObject.activeInHierarchy)
                {
                    Rect rect = GUI2dRectWithObject(child.gameObject, c);
                    Debug.Log("position:" + rect.xMin + ", " + rect.yMin + ", " + rect.xMax + ", " + rect.yMax + ", " + rect.height + ", " + rect.width);
                    
                    Annotation annotation = new Annotation();
                    annotation.id = (num_camera + 1) * 1000000000 + child.gameObject.GetInstanceID();
                    annotation.image_id = num_camera;
                    
                    float[] bbox = new float[4];
                    bbox[0] = rect.xMin;
                    bbox[1] = rect.yMin;
                    bbox[2] = rect.width;
                    bbox[3] = rect.height;
                    annotation.bbox = bbox;
                    annotation.area = rect.width * rect.height;
                    
                    cocoDataset._CocoData.annotations.Add(annotation);
                }
            }
            num_camera++;
        }
        cocoDataset.SaveIntoJson();
    }
    
    public static Rect GUI3dRectWithObject(GameObject go)
    {

        Vector3 cen = go.GetComponent<Renderer>().bounds.center;
        Vector3 ext = go.GetComponent<Renderer>().bounds.extents;
        Vector2[] extentPoints = new Vector2[8]
        {
                WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z-ext.z)),
                WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z-ext.z)),
                WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y-ext.y, cen.z+ext.z)),
                WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y-ext.y, cen.z+ext.z)),
                WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z-ext.z)),
                WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z-ext.z)),
                WorldToGUIPoint(new Vector3(cen.x-ext.x, cen.y+ext.y, cen.z+ext.z)),
                WorldToGUIPoint(new Vector3(cen.x+ext.x, cen.y+ext.y, cen.z+ext.z))
        };
        Vector2 min = extentPoints[0];
        Vector2 max = extentPoints[0];
        foreach (Vector2 v in extentPoints)
        {
            min = Vector2.Min(min, v);
            max = Vector2.Max(max, v);
        }
        return new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
    }

    public static Rect GUI2dRectWithObject(GameObject go, Camera camera=null)
    {
        Vector3[] vertices = go.GetComponent<MeshFilter>().mesh.vertices;

        float x1 = float.MaxValue, y1 = float.MaxValue, x2 = 0.0f, y2 = 0.0f;

        foreach (Vector3 vert in vertices)
        {
            Vector2 tmp = WorldToGUIPoint(go.transform.TransformPoint(vert), camera);

            if (tmp.x < x1) x1 = tmp.x;
            if (tmp.x > x2) x2 = tmp.x;
            if (tmp.y < y1) y1 = tmp.y;
            if (tmp.y > y2) y2 = tmp.y;
        }

        Rect bbox = new Rect(x1, y1, x2 - x1, y2 - y1);
        return bbox;
    }

    public static Vector2 WorldToGUIPoint(Vector3 world, Camera camera=null)
    {
        if (camera == null)
        {
            camera = Camera.main;
        }
        
        Vector2 screenPoint = camera.WorldToScreenPoint(world);
        // screenPoint.y = (float)Screen.height - screenPoint.y;
        screenPoint.y = (float)768 - screenPoint.y;
        return screenPoint;
    }
}