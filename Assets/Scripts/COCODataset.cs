﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COCODataset : MonoBehaviour
{
    [SerializeField] public CocoData _CocoData = new CocoData();

    public void SaveIntoJson(){
        string cocoData = JsonUtility.ToJson(_CocoData);
        String dir = Application.dataPath + "/../CameraScreenshots";
        System.IO.File.WriteAllText(dir + "/cocoData.json", cocoData);
    }
}

[System.Serializable]
public class CocoData
{
    public Info info = new Info();
    public List<CocoImage> images = new List<CocoImage>();
    public List<Annotation> annotations = new List<Annotation>();
    public List<License> licenses = new List<License>();
}

[System.Serializable]
public class Info
{
    public int year;
    public string version;
    public string description;
    public string contributor;
    public string url;
    public DateTime date_created;
}

[System.Serializable]
public class License
{
    public int id;
    public string name;
    public string url;
}

[System.Serializable]
public class Category
{
    public int id;
    public string name;
    public string supercategory;
}

[System.Serializable]
public class CocoImage
{
    public int id;
    public int width;
    public int height;
    public string file_name;
    public int license;
    public string flickr_url;
    public string coco_url;
    public DateTime date_captured;
}

[System.Serializable]
public class Annotation
{
    public int id;
    public int image_id;
    public int category_id;
    public string segmentation;
    public float area;
    public float[] bbox;
    public int iscrowd;
}