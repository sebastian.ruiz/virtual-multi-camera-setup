﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calibrate : MonoBehaviour
{
    
    public GameObject Objects;
    public GameObject Checkerboard;
    public GameObject Aprilboard;
    public KeyCode calibrateKeyAprilboard;
    public KeyCode calibrateKeyCheckerboard;
    
    // Start is called before the first frame update
    void Start()
    {
        Objects.SetActive(true);
        Checkerboard.SetActive(false);
        Aprilboard.SetActive(false);
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(calibrateKeyAprilboard))
        {
            StartCoroutine(CalibrateCameras(Aprilboard, "aprilboardCalibration", true));
        } else if (Input.GetKeyDown(calibrateKeyCheckerboard))
        {
            StartCoroutine(CalibrateCameras(Checkerboard, "checkerboardCalibration", false));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator CalibrateCameras(GameObject board, String directory = "", bool inNanoseconds = true)
    { 
        Debug.Log("Generating camera calibration images");
        // show checkerboard
        Objects.SetActive(false);
        board.SetActive(true);

        // configure rotations
        List<(Vector3 rotation, Vector3 position)> rotpos = new List<(Vector3 rotation, Vector3 postion)>()
        {
            (new Vector3(0, 0, 0), new Vector3(0f, 0.1f, 0f)),
            
            (new Vector3(5, 0, 0), new Vector3(0f, 2.1f, 0f)),
            (new Vector3(0, 5, 0), new Vector3(0f, 2.1f, 0f)),
            (new Vector3(0, 0, 5), new Vector3(0f, 2.1f, 0f)),
            
            (new Vector3(-5, 0, 0), new Vector3(0f, 2.1f, 1f)),
            (new Vector3(0, -5, 0), new Vector3(0f, 2.1f, 1f)),
            (new Vector3(0, 0, -5), new Vector3(0f, 2.1f, 1f)),
            
            (new Vector3(10, 0, 0), new Vector3(0f, 2.1f, 0f)),
            (new Vector3(0, 10, 0), new Vector3(0f, 2.1f, 0f)),
            (new Vector3(0, 0, 10), new Vector3(0f, 2.1f, 0f)),
            
            (new Vector3(-10, 0, 0), new Vector3(0f, 2.1f, 0f)),
            (new Vector3(0, -10, 0), new Vector3(0f, 2.1f, 0f)),
            (new Vector3(0, 0, -10), new Vector3(0f, 2.1f, 0f)),
            
            (new Vector3(15, 0, 0), new Vector3(1f, 2.5f, 1f)),
            (new Vector3(0, 15, 0), new Vector3(1f, 2.5f, 1f)),
            (new Vector3(0, 0, 15), new Vector3(1f, 2.5f, 1f)),

            (new Vector3(20, 0, 0), new Vector3(-1f, 3.0f, -1f)),
            (new Vector3(0, 20, 0), new Vector3(-1f, 3.0f, -1f)),
            (new Vector3(0, 0, 20), new Vector3(-1f, 3.0f, -1f)),
            
            (new Vector3(-20, 0, 0), new Vector3(-1f, 3.0f, -1f)),
            (new Vector3(0, -20, 0), new Vector3(-1f, 3.0f, -1f)),
            (new Vector3(0, 0, -20), new Vector3(-1f, 3.0f, -1f)),
            
            (new Vector3(15, 5, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(5, 0, 15), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, 15, 5), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(5, 5, 15), new Vector3(0f, 3.5f, 0f)),
            
            (new Vector3(-15, 5, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(5, 0, -15), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, 15, -5), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(-5, -5, 15), new Vector3(0f, 3.5f, 0f)),
            
            (new Vector3(5, -5, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(-5, 0, 5), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, 5, -5), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(5, -5, -5), new Vector3(0f, 3.5f, 0f)),

            (new Vector3(-15, 15, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(15, 0, -15), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, -15, 15), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(15, -15, 15), new Vector3(0f, 3.5f, 0f)),

            (new Vector3(-12, 10, 0), new Vector3(1f, 3.5f, 0f)),
            (new Vector3(7, 0, -13), new Vector3(0f, 3.5f, 1f)),
            (new Vector3(0, -5, 11), new Vector3(1f, 3.5f, 0f)),
            (new Vector3(3, -6, 9), new Vector3(0f, 3.5f, 1f)),
            
            (new Vector3(-4, 0, 11), new Vector3(0.5f, 3.5f, 1f)),
            (new Vector3(-7, 2, 0), new Vector3(1f, 3.5f, 0.5f)),
            (new Vector3(0, 7, -13), new Vector3(0.5f, 3.5f, 1f)),
            (new Vector3(-3, 6, 9), new Vector3(1f, 3.5f, 0.5f)),

            (new Vector3(7, -4, 0), new Vector3(0.4f, 3.5f, 0.4f)),
            (new Vector3(-7, 0, 10), new Vector3(0.4f, 3.5f, 0.4f)),
            (new Vector3(0, -10, 7), new Vector3(0.4f, 3.5f, 0.4f)),
            (new Vector3(7, 10, 7), new Vector3(0.4f, 3.5f, 0.4f)),
            
            (new Vector3(6, 6, 0), new Vector3(0.2f, 3.5f, 0.2f)),
            (new Vector3(6, 0, 6), new Vector3(0.2f, 3.5f, 0.2f)),
            (new Vector3(0, 6, 6), new Vector3(0.2f, 3.5f, 0.2f)),
            (new Vector3(6, 6, 6), new Vector3(0.2f, 3.5f, 0.2f)),
            
            (new Vector3(8, -8, 0), new Vector3(-0.2f, 3.5f, -0.2f)),
            (new Vector3(-8, 0, -8), new Vector3(-0.2f, 3.5f, -0.2f)),
            (new Vector3(0, -8, -8), new Vector3(-0.2f, 3.5f, -0.2f)),
            (new Vector3(-8, -8, -8), new Vector3(-0.2f, 3.5f, -0.2f)),

            (new Vector3(5, -5, 0), new Vector3(-0.2f, 3.5f, -0.2f)),
            (new Vector3(-5, 0, 5), new Vector3(-0.2f, 3.5f, -0.2f)),
            (new Vector3(0, -5, -5), new Vector3(-0.2f, 3.5f, -0.2f)),
            (new Vector3(-5, -5, 5), new Vector3(-0.2f, 3.5f, -0.2f)),
            
            (new Vector3(15, 15, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(15, 0, 15), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, 15, 15), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(15, 15, 15), new Vector3(0f, 3.5f, 0f)),
            
            (new Vector3(10, 10, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(10, 0, 10), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, 10, 10), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(10, 10, 10), new Vector3(0f, 3.5f, 0f)),
            
            (new Vector3(-10, -10, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(-10, 0, -10), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, -10, -10), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(-10, -10, -10), new Vector3(0f, 3.5f, 0f)),
            
            (new Vector3(7, -7, 0), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(-7, 0, 7), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(0, 7, -7), new Vector3(0f, 3.5f, 0f)),
            (new Vector3(7, -7, 7), new Vector3(0f, 3.5f, 0f))
        };
        
        
        CameraCapture cameraCapture = gameObject.GetComponent<CameraCapture>();
        cameraCapture.ResetCounter();
        
        for (int i = 0; i < rotpos.Count; i++)
        {
            board.transform.position = rotpos[i].position;
            board.transform.eulerAngles = rotpos[i].rotation;
            yield return new WaitForSeconds(0.1f);
            cameraCapture.Capture(false, directory, inNanoseconds);
        }

        // finished, hide checkboard
        board.SetActive(false);
        Objects.SetActive(true);
        
        Debug.Log("Finished generating calibration images.");
    }
    
}
