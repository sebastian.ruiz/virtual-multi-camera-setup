# Virtual Multi-Camera Setup

This is a virtual multi-camera setup created in Unity. It allows for capturing images of a synthetic scene.

## Features

- Instance segmentation of scene objects
- Calibration using checkerboard

## How to

Run the scene. Then:

- Press the 'S' key to generate images from all 5 cameras.
- Press the 'C' key to generate the calibration images.
